console.log(window.location.search);	

let params = new URLSearchParams(window.location.search);

console.log(...params);
console.log(params.has('courseId'));
console.log(params.get('courseId'));
let courseId = params.get('courseId');
console.log(courseId);

let token = localStorage.getItem('token')

fetch(`http://localhost:4000/api/courses/${courseId}`, {
	method: 'DELETE',
	headers: {
	       'Authorization': `Bearer ${token}`
	}
}).then(res => {
	return res.json()
}).then(data => {
	if(data === true){
		Swal.fire({
			icon: 'success',
			title: 'Course Successfully Disabled!',
			text: 'You have successfully disabled the course.'
		}).then(() => window.location.replace('./courses.html'))
	}else{
		Swal.fire({
					icon: 'error',
					title: 'Error',
					text: 'The selected course is not disabled. Please try again.'
				})
	}
})
