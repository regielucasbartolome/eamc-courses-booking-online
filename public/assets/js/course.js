console.log('Hello')

//The first thing that we need to do is to identify whih course it needs to display inside the browser.
//we are going to use the course id to identify the correct course properly.
let params = new URLSearchParams(window.location.search)
//windows.location -> returns a location object with information about the "current" location of the document
//.search => contains the query string section of the current URL.
//search property returns an object of type stringString
//URLSearchParams() -> this method/constructor creates and returns a URLSearchParams object. (this is a class)
//"URLSearchParams" -> describes the interface that defines utility methods to work with the query string of a URL (this is a prop type/template)
//new -> instantiates a user-defined object.

/*params = {
	"courseId": "id ng course that we passed"
}*/
let id = params.get('courseId'); //courseId can be kahitAno

console.log(id)

//lets capture the access token from the local storage.
let token = localStorage.getItem('token');
console.log(token); //added only for educational purposes

//lets capture the sections of the html body
let name  = document.querySelector("#courseName");
let desc  = document.querySelector("#courseDesc");
let price  = document.querySelector("#coursePrice");
let enroll = document.querySelector("#enrollmentContainer");
let isAdmin = localStorage.getItem('isAdmin');

/*
fetch(`https://radiant-tundra-86932.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(data => {
	console.log(data)
	name.innerHTML = data.name
	desc.innerHTML = data.description
	price.innerHTML = data.price
	enroll.innerHTML = `<button type="submit" class="btn btn-success text-white btn-block">Enroll</button>`;//added id submit
	//if the result is displayed in the console. It means you were able to properly pass teh courseId and successfully created your fetch request.
	enroll.addEventListener('submit', (e) => {
			e.preventDefault();

			fetch("https://radiant-tundra-86932.herokuapp.com/api/users/enroll", {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				},
				body: JSON.stringify({'courseId': `${id}`})
			}).then(res => {return res.json()
			}).then(data => {
				console.log(data);
				if(data === true) {
					Swal.fire({
						icon: 'success',
						title: 'Enrollment Successful',
						message: `Congratulations! You are now officially enrolled to ${name}.`
					});
				}else {
					Swal.fire({
						icon: 'error',
						title: 'Failed to Enroll the Course',
						message: 'Something went wron with the enrollment. Please try again.'
					})
				}
			})
		})
})*/

//what we are going to do here is to insert the enroll button component inside this page. Line 38


//Sir Marty's Approach

fetch(`https://radiant-tundra-86932.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(data => {
	console.log(data)
	name.innerHTML = data.name
	desc.innerHTML = data.description
	price.innerHTML = data.price
	enroll.innerHTML = `<a id="enrollButton" class="btn btn__enroll btn-block">Enroll</button>`; //HTML element form in course.html should be changed also to span

	//we have to capture first the anchor tag for enroll, add and event listener to trigger an event. create a function in the event listener method to describe the next set of procedures.
	document.querySelector('#enrollButton').addEventListener('click', () => {
		//insert the course to our enrollments array inside the user collection
		fetch('https://radiant-tundra-86932.herokuapp.com/api/users/enroll', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({ courseId: id })
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			//now we can inform the user that the request has been done or not.
			if(data === true){
				alert('Thank you for enrolling to this course.')
				//redirect back to courses page
				window.location.replace('./courses.html')
			}else{
				//inform the user that the request has failed
				alert('Somethin went wrong.')
			}
		})
	})
})