//console.log('profile')

//get the access token in the localStorage
let token = localStorage.getItem("token");
console.log(token);

let profileContainer = document.querySelector('#profileContainer');

//lets create a control structure that will determine the display if the access token is null or empty

if(!token || token === null) {
	//lets redirect the user to the login page
	alert("You must login first");
	window.location.href="./login.html"
}else {
	//console.log('yes may nakuhang token') //for educational purposes only
	fetch('https://radiant-tundra-86932.herokuapp.com/api/users/details', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	}).then(res => res.json()).then(data => {
		console.log(data); //checking purposes
		//console.log(data.enrollments[5].courseId)
		let enrollmentData = data.enrollments.map(classData => {
			console.log(classData);
		let courseId = classData.courseId;
			
		fetch(`http:localhost:4000/api/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {console.log(data)})
		
			return(
				`
				<tr>
					<td>${courseId}</td>
					<td>${classData.enrolledOn}</td>
					<td>${classData.status}</td>
				</tr>
				`
			)
		}).join("")//remove the commas

		profileContainer.innerHTML = `
			<div class="col-md-12">
				<section class="jumbotron my-5">
					<h3 >First Name: <strong>${data.firstName}</strong></h3>
					<h3 >Last Name: <strong>${data.lastName}</strong></h3>
					<h5 >Email: <strong>${data.email}<strong/></h5>
					<table class="table table-striped table-dark table-hover">
						<thead>
							<tr>
								<th>Course ID</th>
								<th>Enrolled On</th>
								<th>Status</th>
								<tbody>${enrollmentData}</tbody>
							</tr>
						</thead>
					</table>
				</section>
			</div>


		`
	})
}

